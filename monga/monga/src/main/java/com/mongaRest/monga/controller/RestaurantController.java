package com.mongaRest.monga.controller;

import com.mongaRest.monga.bean.RestaurantCreateBean;
import com.mongaRest.monga.bean.RestaurantSearchBean;
import com.mongaRest.monga.commons.MongaDevConstants;
import com.mongaRest.monga.commons.token.Token;
import com.mongaRest.monga.exception.BadRequestException;
import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import com.mongaRest.monga.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ControllerAPI.RESTAURANT_CONTROLLER + ControllerAPI.VERSION_V1)
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Restaurant> create(
            @RequestHeader(value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token,
            @RequestBody @Valid RestaurantCreateBean createBean
    ) {



        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }

    @GetMapping(value = ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Restaurant> getById(
            @PathVariable(name = "id") Long id
    ) throws BadRequestException {

        Restaurant restaurant = restaurantService.getById(id);

        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @GetMapping(value = ControllerAPI.RESTAURANT_CONTROLLER_SEARCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Restaurant>> search(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestBody @Valid RestaurantSearchBean searchBean
    ) {

        page = page == null ? 0 : page - 1;
        size = size == null ? MongaDevConstants.RESTAURANTS_PAGE_SIZE : size;

        List<Restaurant> search = restaurantService.search(searchBean, page, size);

        Page<Restaurant> restaurantPage;

        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
