package com.mongaRest.monga.config.security.jwt;

import com.mongaRest.monga.pojo.RoleName;
import com.mongaRest.monga.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    public JwtUserFactory() {

    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.getBlocked(),
                user.getUpdatedDate(),
                mapToGrantedAuthority(new ArrayList<>(user.getUserRoles()))
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthority(List<RoleName> userRoles) {
        return userRoles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toList());
    }
}
