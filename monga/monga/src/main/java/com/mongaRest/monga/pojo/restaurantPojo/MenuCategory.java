package com.mongaRest.monga.pojo.restaurantPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class MenuCategory {

    // TODO add Const type ENUM Category

    @JsonProperty
    private String name;

    @JsonProperty
    private List<MenuEatPosition> positions;
}
