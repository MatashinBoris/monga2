package com.mongaRest.monga.controller;

import com.mongaRest.monga.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
@AutoConfigureMockMvc
@Slf4j
public class UserControllerTestBasic {

    @Autowired
    private MockMvc mockMvc;

    protected String performGetUserWithStatus(User client, Long id, ResultMatcher status) throws Exception {
        String request = ControllerAPI.USER_CONTROLLER +  ControllerAPI.VERSION_V1 + ControllerAPI.USER_CONTROLLER_GET+ ControllerAPI.CONTROLLER_SPECIFIC_REQUEST;

        return mockMvc.perform(get(request, id)
                //.header(Token.TOKEN_HEADER))
        ).andExpect(status)
                .andReturn().getResponse().getContentAsString();
    }

}
