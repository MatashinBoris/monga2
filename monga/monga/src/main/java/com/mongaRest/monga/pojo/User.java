package com.mongaRest.monga.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.commons.DateDeserializer;
import com.mongaRest.monga.commons.DateSerializer;
import com.mongaRest.monga.mongoDb.BaseDocument;
import com.mongaRest.monga.mongoDb.BaseDocumentIdSerializer;
import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import com.mongaRest.monga.serialization.RestaurantFromIdDeserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;


@Document(collection = "user")
@TypeAlias("user")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class User extends BaseDocument implements Serializable {

    public static final String FIRST_NAME_FIELD = "firstName";
    public static final String LAST_NAME_FIELD = "lastName";
    public static final String LOGIN_FIELD = "login";
    public static final String PASSWORD_FIELD = "password";
    public static final String ROLE_FIELD = "role";
    public static final String BIRTHDATE_FIELD = "birthDate";

    @JsonProperty
    private String nickName;

    @JsonProperty
    private String login;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonProperty
    private String userName;

    @JsonProperty
    private String firstName;

    @JsonProperty
    private String lastName;

    @JsonIgnore
    private boolean isPaid = true;

    @JsonProperty
    private Boolean confirmed = true;

    @JsonProperty
    private String avatar;

    @JsonProperty
    private Boolean blocked = false;

    @JsonProperty
    private String phone;

    @JsonProperty
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    protected OffsetDateTime birthDate;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Set<RoleName> userRoles = new HashSet<>();

    @DBRef
    @JsonProperty
    @JsonSerialize(using = BaseDocumentIdSerializer.class)
    @JsonDeserialize(using = RestaurantFromIdDeserializer.class)
    private Set<Restaurant> restaurants;

    public boolean hasAnyRole(RoleName... roles) {

        for (RoleName role : roles) {
            if (this.userRoles.contains(role)) {
                return true;
            }
        }
        return false;
    }
}
