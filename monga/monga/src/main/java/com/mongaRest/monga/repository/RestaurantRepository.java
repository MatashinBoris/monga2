package com.mongaRest.monga.repository;

import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RestaurantRepository extends MongoRepository<Restaurant, Long>, CustomRestaurantRepository {

    Optional<Restaurant> findByName(String value);

    Optional<Restaurant> findById(Long id);
}
