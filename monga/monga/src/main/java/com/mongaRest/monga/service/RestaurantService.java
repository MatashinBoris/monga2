package com.mongaRest.monga.service;

import com.mongaRest.monga.bean.RestaurantSearchBean;
import com.mongaRest.monga.exception.BadRequestException;
import com.mongaRest.monga.exception.ValidationException;
import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import com.mongaRest.monga.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    public Restaurant getById(Long id) throws BadRequestException {

        Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(id);

        if (optionalRestaurant.isEmpty()){
            throw new BadRequestException("Restaurant is not exist");
        }

        return optionalRestaurant.get();
    }

    public List<Restaurant> search(RestaurantSearchBean searchBean, Integer page, Integer pageSize) {

        if (page < 0 || pageSize <= 0) {
            throw new ValidationException("Page or page zine can't be low then 1");
        }

        return null;
    }
}
