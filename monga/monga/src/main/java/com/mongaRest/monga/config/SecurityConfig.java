package com.mongaRest.monga.config;


import com.mongaRest.monga.config.security.jwt.JwtConfigurer;
import com.mongaRest.monga.config.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static com.mongaRest.monga.controller.ControllerAPI.REVIEW_CONTROLLER;

@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

    private final JwtTokenProvider jwtTokenProvider;

    private static final String ADMIN_ENDPOINT = "/api/v1/admin/**";
    private static final String LOGIN_ENDPOINT = "/api/v1/auth/login";

    @Autowired
    public SecurityConfig(JwtTokenProvider jwtTokenProvider){
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

/*    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/

   /* @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/api/auth/test/**");

    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic().disable()
                .cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(REVIEW_CONTROLLER + "/ok").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/api/v1/auth/create").permitAll()
                //.antMatchers("/api/users/**").hasRole("USER")
                .antMatchers("/api/users/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));

/*
        http
                .cors().and().csrf()
                .disable();*/

        /*http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
       // http.addFilterBefore(coreModuleUserDetailsService, CoreModuleUserDetailsService.class);


        http.antMatcher("/api/**")
                .authorizeRequests()
                .antMatchers("/api/auth/login**").permitAll()
                .antMatchers("/api/users/**").hasRole("USER")
                .antMatchers("/api/auth/logout").permitAll()
                .antMatchers("/api/users/get").permitAll()
                .antMatchers("/api/borders").permitAll()
                .antMatchers("/api/dispatcher/**").hasAnyRole("ADMIN", "DISPATCHER", "USER")
                .antMatchers("/api/statistics/**").hasAnyRole("ADMIN", "DISPATCHER")
                .antMatchers("/api/geocoding/**").permitAll()
                .antMatchers("/api/departments/**").authenticated()
                .and()
                .authorizeRequests().anyRequest().hasRole("ADMIN");*/
    }


    /*   @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
//		config.setAllowedOrigins(Collections.singletonList("*"));
        config.setAllowedOriginPatterns(Collections.singletonList("*"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }*/
}
