package com.mongaRest.monga.controller;

import com.mongaRest.monga.commons.JacksonUtils;
import com.mongaRest.monga.config.CoreModuleUserDetailsService;
import com.mongaRest.monga.config.CoreMongoConfig;
import com.mongaRest.monga.config.SecurityConfig;
import com.mongaRest.monga.config.security.jwt.JwtTokenProvider;
import com.mongaRest.monga.pojo.RoleName;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import com.mongaRest.monga.service.UserService;
import com.mongaRest.monga.test_utils.UserUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {UserController.class, UserService.class, UserUtils.class,
        CoreMongoConfig.class, SecurityConfig.class, BCryptPasswordEncoder.class, JwtTokenProvider.class,
        CoreModuleUserDetailsService.class})
@AutoConfigureMockMvc
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@EnableAutoConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class UserControllerTest extends UserControllerTestBasic{

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    private User user;

    @Autowired
    private UserUtils userUtils;

    @BeforeEach
    public void before() {
         user = new User().setUserRoles(Set.of(RoleName.USER)).setFirstName("firstName").setLastName("lastName").setLogin("12345@gmail.com")
                .setPassword("password");
         user.setId(15L);
        userRepository.save(user);

    }

    @Test
    public void test() throws Exception {
        assertThat(userController).isNotNull();

        User user = userUtils.createAndSaveUser(RoleName.USER);
        //userRepository.save(user.setPassword(passwordEncoder.encode(user.getPassword())));

        Long id = 15L;
        String body = performGetUserWithStatus(user, user.getId(), status().isOk());

        User user2 = JacksonUtils.fromJson(User.class, body);


        assertNotNull(user2);
        assertEquals("Boris", user2.getFirstName());
    }


}
