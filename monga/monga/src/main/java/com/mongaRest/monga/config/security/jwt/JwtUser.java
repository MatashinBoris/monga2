package com.mongaRest.monga.config.security.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.OffsetDateTime;
import java.util.Collection;

public class JwtUser implements UserDetails {

    private final Long id;
    private final String userName;
    private final String firstName;
    private final String lastName;
    private final String password;
    private final Boolean blocked;
    private final OffsetDateTime lastResetPassword;
    private final Collection<? extends GrantedAuthority> authorities;

    public JwtUser(Long id, String userName, String firstName, String lastName, String password, Boolean blocked, OffsetDateTime lastResetPassword, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.blocked = blocked;
        this.lastResetPassword = lastResetPassword;
        this.authorities = authorities;
    }

    @JsonIgnore
    public Long getId() {return id;}

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public OffsetDateTime getLastResetPassword() {
        return lastResetPassword;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return blocked;
    }
}
