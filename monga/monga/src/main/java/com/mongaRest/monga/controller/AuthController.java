package com.mongaRest.monga.controller;

import com.mongaRest.monga.bean.AuthBean;
import com.mongaRest.monga.config.security.jwt.JwtTokenProvider;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import com.mongaRest.monga.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@RestController
@RequestMapping(ControllerAPI.AUTH_CONTROLLER + ControllerAPI.VERSION_V1)
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider tokenProvider;

    private final UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, JwtTokenProvider tokenProvider, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userRepository = userRepository;
    }

    @PostMapping("")
    public ResponseEntity<User> createUser(
    ) {
        User user = new User().setUserName("Pojo user name");

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authenticate(@RequestBody AuthBean request) {
        try{
            String login = request.getEmail();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, request.getPassword().trim().toLowerCase()));
            User user = userRepository.findByLogin(login);

            if (user == null){
                throw new UsernameNotFoundException("User with login:" + login + " , not found");
            }

            String token = tokenProvider.createToken(user.getId(), user.getLogin(), new ArrayList<>(user.getUserRoles()));

            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            throw new BadCredentialsException("Invalid login or password");
        }
    }

    @PostMapping("/logout")
    public void logout(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}
