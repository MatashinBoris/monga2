package com.mongaRest.monga.commons.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public class InThePastValidator implements ConstraintValidator<InThePast, OffsetDateTime> {

    public final void initialize(final InThePast annotation) {
    }

    public final boolean isValid(final OffsetDateTime value, final ConstraintValidatorContext context) {

        if (value == null) {
            return true;
        }

        var today = OffsetDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);

        return value.isBefore(today);
    }
}