package com.mongaRest.monga.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.pojo.GeoPosition;
import com.mongaRest.monga.pojo.restaurantPojo.KitchenType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
@Accessors(chain = true)
public class RestaurantSearchBean {

    @JsonProperty
    private String searchQuery;

    @JsonProperty
    private Sort.Direction sortDirection;

    @JsonProperty
    private String name;

    @JsonProperty
    private String city;

    @JsonProperty
    private String address;

    @JsonProperty
    private GeoPosition position;

    @JsonProperty
    private int radiusKmByPosition;

    @JsonProperty
    private List<KitchenType> kitchenTypes;

    @JsonProperty
    private int averageCheck;

    @JsonProperty
    private double rating;

}
