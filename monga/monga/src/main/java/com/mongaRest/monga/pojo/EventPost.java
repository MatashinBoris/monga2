package com.mongaRest.monga.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.mongoDb.BaseDocument;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "eventPost")
@TypeAlias("eventPost")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class EventPost extends BaseDocument implements Serializable {

    private PostType postType;

    @JsonProperty
    private String imagePath;

    @JsonProperty
    private String title;

    @JsonProperty
    private OffsetDateTime start;

    @JsonProperty
    private OffsetDateTime end;

    @JsonProperty
    private String theme;

    @JsonProperty
    private List<EventType> eventType = new ArrayList<>();

    @JsonProperty
    private String text;

    @JsonProperty
    private List<Long> restaurantsId = new ArrayList<>();

    @JsonProperty
    private boolean active = true;

    @JsonProperty
    private boolean deleted = false;

    @JsonProperty
    private User creator;

}
