package com.mongaRest.monga.pojo.restaurantPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@Getter
@Setter
@Accessors(chain = true)
@ToString
public class MenuEatPosition extends MenuPosition {

    @JsonProperty
    private double weight;

    @JsonProperty
    private double calories;

}
