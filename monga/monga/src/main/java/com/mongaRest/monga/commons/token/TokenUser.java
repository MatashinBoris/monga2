package com.mongaRest.monga.commons.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.pojo.RoleName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class TokenUser {

    public static final String ID_FIELD = "id";
    public static final String LOGIN_FIELD = "login";
    public static final String ROLE_FIELD = "userrole";

    @JsonProperty
    private Long id;

    @JsonProperty
    private String login;

    @JsonProperty
    private Set<RoleName> roleName;
}