package com.mongaRest.monga.pojo.restaurantPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class MenuPosition {

    @JsonProperty
    private String name;

    @JsonProperty
    private double price;

    @JsonProperty
    private List<String> ingredients;

    @JsonProperty
    private String photo;

    @JsonProperty
    private String description;
}
