package com.mongaRest.monga.pojo;

public enum RoleName {
    USER, OWNER, MODERATOR, ADMIN
}
