package com.mongaRest.monga.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.mongaRest.monga.commons.ApplicationContextWrapper;
import com.mongaRest.monga.exception.ValidationException;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.io.IOException;

@Component
public class UserFromIdDeserializer extends JsonDeserializer<User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        //if (userRepository == null) {
        userRepository = ApplicationContextWrapper.getBean(UserRepository.class);
        //}

        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

        JsonToken currentToken = jsonParser.getCurrentToken();
        String currentName = jsonParser.getCurrentName();
        var value = jsonParser.getValueAsString();

        if (currentToken.equals(JsonToken.VALUE_STRING)) {
            if (StringUtils.isNumeric(value)) {
                return userRepository.findById(Long.valueOf(value)).orElseThrow(() -> new ValidationException(currentName, "Cannot get user from the database by id %s", value));
            } else {
                throw new ValidationException(currentName, "User id is not numeric %s", value);
            }
        } else if (currentToken.equals(JsonToken.VALUE_NULL)) {
            return null;
        }

        throw new ValidationException(currentName, "Only string values supported. Value was %s", jsonParser.getText());
    }

}
