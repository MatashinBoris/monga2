package com.mongaRest.monga.mongoDb;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.commons.DateDeserializer;
import com.mongaRest.monga.commons.DateSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@EqualsAndHashCode()
public class BaseDocument implements IdentifiableDocument {

    public static final String ID_FIELD = "id";

    public static final String CREATED_DATE_FIELD = "createdDate";

    public static final String UPDATED_DATE_FIELD = "updatedDate";

    @Id
    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    protected Long id;

    @CreatedDate
    @JsonProperty
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    protected OffsetDateTime createdDate;

    @LastModifiedDate
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    protected OffsetDateTime updatedDate;

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof BaseDocument && id != null) {
            return id.equals(((BaseDocument) object).getId());
        }
        return false;
    }
}
