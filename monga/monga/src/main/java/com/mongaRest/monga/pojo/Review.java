package com.mongaRest.monga.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.mongoDb.BaseDocument;
import com.mongaRest.monga.mongoDb.BaseDocumentIdSerializer;
import com.mongaRest.monga.serialization.UserFromIdDeserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "review")
@TypeAlias("review")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class Review extends BaseDocument implements Serializable {


    @JsonProperty
    private String text;

    @JsonProperty
    private double rating;

    @JsonProperty
    @JsonSerialize(using = BaseDocumentIdSerializer.class)
    @JsonDeserialize(using = UserFromIdDeserializer.class)
    private User user;
}
