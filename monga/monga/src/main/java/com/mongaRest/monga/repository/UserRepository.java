package com.mongaRest.monga.repository;

import com.mongaRest.monga.pojo.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, Long>{

    User findByLogin(String login);

    User findByNickName(String nickName);
}
