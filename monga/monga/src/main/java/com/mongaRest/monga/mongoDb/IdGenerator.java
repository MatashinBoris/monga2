package com.mongaRest.monga.mongoDb;

public class IdGenerator {

    protected final static int SEQ_NUM_BITS = 9;

    protected final static int NODE_NUM_BITS = 10;

    protected final static int BACK_TIME_BITS = 1;

    protected final static long SEQ_NUM_MAX = 1 << SEQ_NUM_BITS;

    protected static volatile int seqNum = 0;

    // TODO implement requesting node number on startup. 2 in power of
    // NODE_NUM_BITS nodes supported
    protected final static int nodeNum = 0;

    protected static volatile long lastTimestamp = 0;

    protected static long backTimeMoment = Long.MAX_VALUE;

    /**
     * Returns next Id. Main method of generator
     *
     * @return
     */
    public synchronized static long nextId() {
        long curTimestamp = System.currentTimeMillis();

        // if time was returned backwards, preparing to turning on conflict free
        // flag
        if (curTimestamp < lastTimestamp) {
            backTimeMoment = lastTimestamp;
            lastTimestamp = curTimestamp;
        } else {
            if (curTimestamp > backTimeMoment) {
                backTimeMoment = Long.MAX_VALUE;
            }
        }

        // if more than 1 identifier was generated per millisecond, increment
        // sequence counter
        if (curTimestamp == lastTimestamp) {
            seqNum++;

            if (seqNum >= SEQ_NUM_MAX) {
                seqNum = 0;
                while (curTimestamp == lastTimestamp) {
                    curTimestamp = System.currentTimeMillis();
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                    }
                }

            }
        } else {
            seqNum = 0;
        }

        // remembering last time when identifier was generated
        lastTimestamp = curTimestamp;

        // generating identifier with all its components
        return curTimestamp << (SEQ_NUM_BITS + NODE_NUM_BITS + BACK_TIME_BITS) | nodeNum << (SEQ_NUM_BITS + BACK_TIME_BITS)
                | (backTimeMoment != Long.MAX_VALUE ? 1 : 0) << SEQ_NUM_BITS | seqNum;

    }
}
