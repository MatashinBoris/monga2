package com.mongaRest.monga.controller;

public class ControllerAPI {

    public static final String CONTROLLER_SPECIFIC_REQUEST = "/{id}";
    public static final String VERSION_V1 = "/v1";

    public static final String USER_CONTROLLER = "/api/users";
    public static final String USER_CONTROLLER_POST_CREATE = "/create";
    public static final String USER_CONTROLLER_GET = "/get";
    public static final String USER_CONTROLLER_DELETE_USER = "/delete";
    public static final String USER_CONTROLLER_BLOCK_USER = "/block";
    public static final String USER_CONTROLLER_UNBLOCK_USER = "/unblock";


    public static final String REVIEW_CONTROLLER = "/api/review";



    public static final String AUTH_CONTROLLER = "/api/review";


    public static final String RESTAURANT_CONTROLLER = "/api/restaurants";
    public static final String RESTAURANT_CONTROLLER_SEARCH = "/api/restaurants/search";

    public static final String FILE_CONTROLLER = "/api/files";
    public static final String FILE_CONTROLLER_FILE_BY_ID = "/byid";


    public static final String EVENT_CONTROLLER = "/api/users";
}
