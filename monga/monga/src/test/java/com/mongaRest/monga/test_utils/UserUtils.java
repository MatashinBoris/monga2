package com.mongaRest.monga.test_utils;


import com.mongaRest.monga.pojo.RoleName;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;

@Component
public class UserUtils {
    private UserRepository userRepository;

    @Autowired
    public UserUtils(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(RoleName role) {
        return new User()
                .setLogin(RandomStringUtils.randomNumeric(10) + "test@aeromap.com")
                .setPhone(RandomStringUtils.randomNumeric(12))
                .setFirstName("Firstname" + RandomStringUtils.randomNumeric(3))
                .setLastName("Lastname" + RandomStringUtils.randomNumeric(3))
                .setPassword(RandomStringUtils.randomAscii(14))
                .setUserRoles(Set.of(role));
    }

   /* public UserSignupBean createUserBean() {
        return new UserSignupBean()
                .setLogin(RandomStringUtils.randomNumeric(10) + "test@aeromap.com")
                .setPhone(RandomStringUtils.randomNumeric(12))
                .setBirthDate(OffsetDateTime.now().minusYears(20).withNano(0))
                .setFirstName("Firstname" + RandomStringUtils.randomNumeric(3))
                .setLastName("Lastname" + RandomStringUtils.randomNumeric(3))
                .setPassword(RandomStringUtils.randomAscii(14));
    }*/

    public boolean equalUsersSkipPasswordAndRole(User a, User b) {
        return a.getId().equals(b.getId())
                && a.getLogin().equals(b.getLogin())
                && a.getPhone().equals(b.getPhone())
                && a.getFirstName().equals(b.getFirstName())
                && a.getLastName().equals(b.getLastName());
    }

    public boolean equalUsers(User a, User b) {
        return equalUsersSkipPasswordAndRole(a, b)
                && a.getPassword().equals(b.getPassword())
                && a.getUserRoles().equals(b.getUserRoles());
    }

    /**
     * Update first name: adds "(updated)" at the beginning
     * also sets values to
     * address, addressAFTN, certifiedAirport, orgnId, birthDate
     *//*
    public UserUpdateBean prepareUpdateOf(User user) {
        UserUpdateBean bean = new UserUpdateBean();
        CustomBeanUtils.copyProperties(user, bean);
        bean.setFirstName("(updated)" + user.getFirstName());
        bean.setAddress("address_" + RandomUtils.nextInt(1, 20));
        bean.setAddressAFTN("12345" + RandomUtils.nextInt(100, 999));
        bean.setOrgnId("Orgn_" + RandomUtils.nextInt(1, 50));

        LocalDate date = LocalDate.of(1970 + RandomUtils.nextInt(0, 30),
                RandomUtils.nextInt(1, 13), RandomUtils.nextInt(1, 25));
        LocalTime time = LocalTime.of(0, 0);
        OffsetDateTime bd = OffsetDateTime.of(date, time, ZoneOffset.UTC);
        bean.setBirthDate(bd);
        return bean;
    }

    public User.Settings prepareUpdateOf(User.Settings settings) {
        Language l = otherThen(settings.getLanguage());
        return new User.Settings(l);
    }

    private Language otherThen(Language language) {
        Language[] all = Language.values();
        int next = language.ordinal() + 1;
        if (next >= all.length) {
            next = 0;
        }
        return all[next];
    }*/

    public User createAndSaveUser(RoleName roleName) {
        var user= createUser(roleName);
        return userRepository.save(user);
    }

    public List<User> createAndSaveUsers(RoleName roleName, int count) {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            users.add(createAndSaveUser(roleName));
        }
        return users;
    }

    public boolean presentIn(User element, List<User> list) {
        for (var i : list) {
            if (equalUsers(i, element)) {
                return true;
            }
        }
        return false;
    }

    public boolean presentIn(User element, List<User> list, BiPredicate<User, User> equal) {
        for (var i : list) {
            if (equal.test(i, element)) {
                return true;
            }
        }
        return false;
    }

    public boolean equalLists(List<User> a, List<User> b) {
        return equalLists(a, b, this::equalUsers);
    }

    public boolean equalLists(List<User> a, List<User> b, BiPredicate<User, User> equal) {
        //b contains a && a contains b
        return a.stream().allMatch(x -> presentIn(x, b, equal))
                && b.stream().allMatch(x -> presentIn(x, a, equal));
    }

}
