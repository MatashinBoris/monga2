package com.mongaRest.monga.mongoDb;

public interface IdentifiableDocument {

    public Long getId();

    public IdentifiableDocument setId(Long id);
}
