package com.mongaRest.monga.mongoDb;

import com.google.common.base.MoreObjects;
import com.google.common.hash.HashCode;
import com.google.common.net.MediaType;
import org.springframework.data.mongodb.gridfs.GridFsResource;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class GridFsResourcePointer implements FilePointer{
    private final GridFsResource target;
    private final HashCode tag;
    private final MediaType mediaType;

    public GridFsResourcePointer(GridFsResource resource) {
        ofNullable(resource).orElseThrow(() -> new IllegalArgumentException("Target of GridFsResourcePointer must not be null"));
        this.target = resource;
        this.tag = HashCode.fromInt(resource.hashCode());
        this.mediaType = resource.getContentType() != null ? MediaType.parse(resource.getContentType()) : null;
    }

    public String getId() {
        return target.getGridFSFile().getId().asObjectId().getValue().toString();
    }

    public InputStream open() {
        try {
            return target.getInputStream();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public long getSize() {
        try {
            return target.contentLength();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getOriginalName() {
        return target.getFilename();
    }

    public String getEtag() {
        return "\"" + tag + "\"";
    }

    public Instant getLastModified() {
        try {
            return toEpochMilli(target.lastModified()).withNano(0).toInstant();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static OffsetDateTime toEpochMilli(long epochMilli) {
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), ZoneOffset.UTC);
    }

    public boolean matchesEtag(String requestEtag) {
        return getEtag().equals(requestEtag);
    }

    public boolean modifiedAfter(Instant clientTime) {
        return !clientTime.isBefore(getLastModified());
    }

    public Optional<MediaType> getMediaType() {
        return ofNullable(mediaType);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("target", target)
                .add("originalName", getOriginalName())
                .add("size", getSize())
                .add("tag", tag)
                .add("mediaType", mediaType)
                .toString();
    }

}
