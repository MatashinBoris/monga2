package com.mongaRest.monga.controller;

import com.mongaRest.monga.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerAPI.REVIEW_CONTROLLER)
public class ReviewController {

    @Autowired
    private UserService userService;

    @GetMapping("/ok")
    public ResponseEntity<String> test() {
       // userService.createUser();
        return new ResponseEntity<>("all is ok", HttpStatus.CREATED);
    }
}
