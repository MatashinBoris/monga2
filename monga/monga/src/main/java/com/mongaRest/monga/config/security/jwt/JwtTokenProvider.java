package com.mongaRest.monga.config.security.jwt;

import com.mongaRest.monga.commons.token.Token;
import com.mongaRest.monga.commons.token.TokenUser;
import com.mongaRest.monga.exception.ValidationException;
import com.mongaRest.monga.pojo.RoleName;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Component
@Slf4j
public class JwtTokenProvider {

    @Value("${jwt.token.secret}")
    private String secret;

    @Value("${jwt.token.expired}")
    private long validityInMilliseconds;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public String createToken(Long id, String login, List<RoleName> roles){

        Claims claims = Jwts.claims().setSubject(login);
        claims.put("roles", getRoleNames(roles));
        claims.put("id", id);

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(secret.getBytes()))
                .compact();

    }

    public TokenUser decodeToken(String token) {
        return new TokenUser().setId(getId(token)).setLogin(getLogin(token)).setRoleName(getUserRoles(token));
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getLogin(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public Long getId(String token) {
        long id;

        try{
            id = Long.parseLong(Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getId());
        } catch (ClassCastException e) {
            log.error("JwtTokenProvider getId() cant get Long id from: %s", Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getId());
            throw new ValidationException("JwtTokenProvider getId() cant get Long id from: %s", Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getId());
        }
        return id;
    }

    public Set<RoleName> getUserRoles(String token) {
        RoleName[] roles = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().get("roles", RoleName[].class);
        return Set.of(roles);
    }

    public String getLogin(String token) {

        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest req) {
        return req.getHeader(Token.TOKEN_HEADER);
    }

    public boolean validateToken(String token) {

        try{
            Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())){
                return false;
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthException("JWT token is expired or invalid");
        }
    }

    private List<String> getRoleNames(List<RoleName> roles) {
        List<String> result = new ArrayList<>();

        roles.forEach(roleName -> {
            result.add(roleName.name());
        });

        return result;
    }
}
