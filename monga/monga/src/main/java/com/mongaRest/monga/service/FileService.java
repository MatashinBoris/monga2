package com.mongaRest.monga.service;

import com.mongaRest.monga.exception.ValidationException;
import com.mongaRest.monga.mongoDb.FilePointer;
import com.mongaRest.monga.mongoDb.GridFsResourcePointer;
import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

@Service
@Slf4j
public class FileService {

    private static final List<String> allowedFileFormats = List.of("png", "jpg", "pdf", "txt");

    private static final String ETAG_FIELD = "_etag";
    private static final int ETAG_LENGTH = 5;
    //it is needed because GridFsResource.CONTENT_TYPE_FIELD is not public
    private static final String CONTENT_TYPE_FIELD = "_contentType";
    private static final String ALPHA_NUMERIC = "alpACEFGHJKLMNPQRUVWXY" + "abcdefhijkprstuvwx" + "0123456789";
    private final Random random = new Random();

    @Autowired
    private GridFsTemplate gridFsTemplate;

    public Optional<GridFSFile> findById(String fileId) {
        ObjectId id = tryParseObjectId(fileId);
        if (id == null) {
            return Optional.empty();
        }
        Query byId = Query.query(GridFsCriteria.where("_id").is(id));
        GridFSFile file = gridFsTemplate.find(byId).first();
        return ofNullable(file);
    }

    public Optional<FilePointer> findFile(String filename) {
        GridFsResource resource = gridFsTemplate.getResource(filename);
        if (resource == null) return Optional.empty();
        try {
            // if does it throw IllegalStateException it means that the resource was deleted but still not null
            resource.getContentType();
            return of(new GridFsResourcePointer(resource));
        } catch (IllegalStateException e) {
            return Optional.empty();
        }
    }

    /**Stores file and closes stream
     * @return id of stored file
     * */
    public String saveFile(InputStream content, String fileName, String contentType) throws ValidationException {
        Document metadata = new Document();


        //	metadata.append(CONTENT_TYPE_FIELD, validMediaType(contentType));
        metadata.append(CONTENT_TYPE_FIELD, contentType);

        metadata.append(ETAG_FIELD, generateETag());

        String id = gridFsTemplate.store(content, fileName, metadata).toString();
        try {
            content.close();
        } catch (IOException e) {
            throw new ValidationException("Failed to save file: " + e.getMessage());
        }
        log.info("Saved file with id %s and name %s", id, fileName);
        return id;
    }

    public String saveFile(MultipartFile file) throws ValidationException {

        if (!isAllowedFileFormat(file.getOriginalFilename())) {
            throw new ValidationException("Invalid file format");
        }

        try {
            return saveFile(file.getInputStream(), file.getOriginalFilename(), file.getContentType());
        } catch (IOException e) {
            throw new ValidationException("Failed to save file: " + e.getMessage());
        }
    }

    private String generateETag() {
        char[] etag = new char[ETAG_LENGTH];
        int randBound = ALPHA_NUMERIC.length();
        for (int i = 0; i < ETAG_LENGTH; i++) {
            int j = random.nextInt(randBound);
            etag[i] = ALPHA_NUMERIC.charAt(j);
        }
        return new String(etag);
    }

    /**
     * @return null if incorrect format
     */
    private ObjectId tryParseObjectId(String id) {
        try {
            return new ObjectId(id);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public Instant getUploadDate(GridFSFile file) {
        return file.getUploadDate().toInstant();
    }

    public String getETag(GridFSFile file) {
        String etag = file.getMetadata().getString(ETAG_FIELD);
        if (etag == null) {
            log.error("File with id = %s does not have etag", file.getId().asObjectId().getValue());
            etag = "";
        }
        return etag;
    }

    public Resource getAsResource(GridFSFile file) {
        return gridFsTemplate.getResource(file);
    }

    public void removeById(String fileId) {
        ObjectId objId = tryParseObjectId(fileId);
        if (objId != null) {
            Query byId = Query.query(GridFsCriteria.where("_id").is(objId));
            gridFsTemplate.delete(byId);
        }
    }

    private boolean isAllowedFileFormat(String fileName) {

        //how to define media type: 1header, 2extention, 3tika(image service)

        String fileExtension = FilenameUtils.getExtension(fileName);

        for (String allowedFileFormat : allowedFileFormats) {
            if (allowedFileFormat.equalsIgnoreCase(fileExtension)) {
                return true;
            }
        }

        return false;
    }

    public MediaType getContentType(GridFSFile file) {
        String contentType = file.getMetadata().getString(CONTENT_TYPE_FIELD);
        MediaType result;
        try {
            result = MediaType.parseMediaType(contentType);
        } catch (InvalidMediaTypeException e) {
            log.error("File with id = %s has invalid content type: %s", file.getId().asObjectId().getValue(), contentType);
            result = MediaType.APPLICATION_OCTET_STREAM;
        }
        return result;
    }
}
