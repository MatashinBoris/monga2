package com.mongaRest.monga.bean;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.commons.DateDeserializer;
import com.mongaRest.monga.commons.DateSerializer;
import com.mongaRest.monga.commons.validator.InThePast;
import com.mongaRest.monga.commons.validator.ValidationConstants;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

@Data
@Accessors(chain = true)
public class UserUpdateBean {

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_FIRST_NAME_MESSAGE)
    @Size(min = 3, max = 50, message = ValidationConstants.USER_NICK_NAME_MESSAGE)
    private String nickName;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_FIRST_NAME_MESSAGE)
    @Size(min = 3, max = 50, message = ValidationConstants.USER_FIRST_NAME_MESSAGE)
    private String firstName;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_LAST_NAME_MESSAGE)
    @Size(min = 3, max = 50, message = ValidationConstants.USER_LAST_NAME_MESSAGE)
    private String lastName;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_PHONE_MESSAGE)
    @Size(min = 10, message = ValidationConstants.USER_PHONE_MESSAGE)
    @Pattern(regexp = ValidationConstants.PHONE_PATTERN, message = ValidationConstants.USER_PHONE_MESSAGE)
    private String phone;

    @JsonProperty
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @InThePast
    @NotNull
    protected OffsetDateTime birthDate;
}
