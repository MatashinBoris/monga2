package com.mongaRest.monga.config.security;

import com.mongaRest.monga.config.security.jwt.JwtUser;
import com.mongaRest.monga.config.security.jwt.JwtUserFactory;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JwtUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public JwtUserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userRepository.findByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("User with username = " + login + " not found");
        }

        JwtUser jwtUser = JwtUserFactory.create(user.setBlocked(true));
        log.info("In loadUserByUsername - user with username: " + login + " , successfully loaded");
        return jwtUser;
    }
}
