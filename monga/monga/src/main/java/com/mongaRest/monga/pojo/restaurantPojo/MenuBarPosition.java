package com.mongaRest.monga.pojo.restaurantPojo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class MenuBarPosition extends MenuPosition{

    private int milliliters;

    private double alcoholСontent;
}
