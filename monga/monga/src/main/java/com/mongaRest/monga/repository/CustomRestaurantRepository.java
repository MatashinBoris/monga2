package com.mongaRest.monga.repository;

import com.mongaRest.monga.bean.RestaurantSearchBean;
import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomRestaurantRepository {

    /**
     * @param search if field is null, it is not taken for filtering
     * @param creatorId if null - not taken for filtering
     * @param page zero-based page number
     * @throws IllegalArgumentException if page < 0 or pageSize <= 0
     */
    Page<Restaurant> search(RestaurantSearchBean search, Long creatorId, int page, int pageSize);
}
