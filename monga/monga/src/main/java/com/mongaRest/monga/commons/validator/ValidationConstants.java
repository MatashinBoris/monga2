package com.mongaRest.monga.commons.validator;

public class ValidationConstants {

    public static final String USER_NICKNAME_MESSAGE = "Incorrect nickName";

    public static final String USER_LOGIN_MESSAGE = "Incorrect login";

    public static final String USER_PASSWORD_MESSAGE = "Min 8 characters";

    public static final String USER_FIRST_NAME_MESSAGE = "Min 3 character";

    public static final String USER_NICK_NAME_MESSAGE = "Min 3 character";

    public static final String USER_LAST_NAME_MESSAGE = "Min 3 character";

    public static final String USER_PHONE_MESSAGE = "Min 7 characters (digits - and +)";

    public static final String PHONE_PATTERN =  "^[\\+]?([0-9]{1,2})?[-\\s]?[(]?[0-9]{3}[)]?[-\\s]?[0-9]{3}[-\\s]?[0-9]{2,4}[-\\s]?[0-9]{0,2}$";

    public static final String IN_THE_PAST_MESSAGE = "Should be in the past";


    public static final int MINIMAL_USER_AGE = 16;

    public static final int MAXIMUM_USER_AGE = 120;

    public static final String MINIMAL_USER_AGE_MESSAGE = "User age too small";

    public static final String MAXIMUM_USER_AGE_MESSAGE = "User age too big";

    public static final String BANNED_EMAIL_SERVICE_MESSAGE = "Banned email service";
}
