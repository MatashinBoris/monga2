package com.mongaRest.monga.pojo.restaurantPojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.mongoDb.BaseDocument;
import com.mongaRest.monga.mongoDb.BaseDocumentIdSerializer;
import com.mongaRest.monga.pojo.GeoPosition;
import com.mongaRest.monga.pojo.Review;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.serialization.UserFromIdDeserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Document(collection = "restaurant")
@TypeAlias("restaurant")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class Restaurant extends BaseDocument implements Serializable {

    public static final String RATING_FIELD = "rating";
    public static final String CREATOR_FIELD = "creator";
    public static final String ACTIVE_FIELD = "active";

    @JsonProperty
    private String name;

    @JsonProperty
    private String city;

    @JsonProperty
    private String address;

    @JsonProperty
    private GeoPosition position;

    @JsonProperty
    private List<KitchenType> kitchenTypes = new ArrayList<>();

    @JsonProperty
    private String phone;

    @JsonProperty
    private String email;

    @JsonProperty
    private String siteUrl;

    @JsonProperty
    private String description;

    @JsonProperty
    private int averageCheck;

    @JsonProperty
    private double rating;

    @JsonProperty
    private List<String> restaurantPhotos;

    @DBRef
    @JsonIgnore
    @JsonSerialize(using = BaseDocumentIdSerializer.class)
    @JsonDeserialize()
    private List<Review> reviews;

    @DBRef
    @JsonIgnore
    @JsonSerialize(using = BaseDocumentIdSerializer.class)
    @JsonDeserialize()
    private Menu menu;

    @DBRef
    @JsonProperty
    @JsonSerialize(using = BaseDocumentIdSerializer.class)
    @JsonDeserialize(using = UserFromIdDeserializer.class)
    protected User creator;

    @JsonProperty
   // @ApiModelProperty(value = "Restaurant is active or not(deleted)")
    private boolean active = true;

    //TODO work time

    //Todo User owner

    //Todo User Moderator

    //Todo list likes


}
