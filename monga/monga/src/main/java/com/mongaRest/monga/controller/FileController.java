package com.mongaRest.monga.controller;

import com.mongaRest.monga.bean.UploadFileBean;
import com.mongaRest.monga.exception.BadRequestException;
import com.mongaRest.monga.exception.ForbiddenException;
import com.mongaRest.monga.service.FileService;
import com.mongaRest.monga.service.UserService;
import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(ControllerAPI.FILE_CONTROLLER + ControllerAPI.VERSION_V1)
@Slf4j
public class FileController {

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;


    @GetMapping(value = ControllerAPI.FILE_CONTROLLER_FILE_BY_ID + ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.ALL_VALUE)
    public ResponseEntity<Resource> getById(@PathVariable("id") String fileId,
                                            @RequestHeader HttpHeaders requestHeaders) throws IOException {
        Optional<GridFSFile> optFile = fileService.findById(fileId);
        return doGet(optFile, requestHeaders);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UploadFileBean> save(
         //   @RequestHeader(name = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token,
            @RequestParam("file") MultipartFile file
    ) throws ForbiddenException, BadRequestException {

        //TokenUser tokenUser = getTokenUserOrThrow(token);
        /*Long editorId = tokenUser.getId();
        User user = userService.getUserById(tokenUser.getId());


        if (!user.hasAnyRole(RoleName.USER, RoleName.OPERATOR, RoleName.MODERATOR, RoleName.ADMIN)) {
            log.info("User with id %s tried to save file", editorId);
            throw new ForbiddenException(new ErrorMessage("Forbidden to upload file"));
        }*/

        String fileId = fileService.saveFile(file);
        Optional<GridFSFile> optFile = fileService.findById(fileId);
        //no atomicity, need to check if file is still present
        if (optFile.isEmpty()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        //prepare response
        GridFSFile fileInfo = optFile.get();

        String etag = fileService.getETag(fileInfo);
        Instant uploadDate = fileService.getUploadDate(fileInfo);
        MediaType contentType = fileService.getContentType(fileInfo);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setETag("\"" + etag + "\"");
        responseHeaders.setLastModified(uploadDate);

       // log.info("User with id %s successful saved file with id %s", editorId, fileId);
        return ResponseEntity.ok().headers(responseHeaders).body(new UploadFileBean(fileId, contentType));
    }

    @DeleteMapping(value = ControllerAPI.FILE_CONTROLLER_FILE_BY_ID
            + ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.ALL_VALUE)
    public ResponseEntity<?> deleteById(@PathVariable("id") String fileId) {
        fileService.removeById(fileId);
        return ResponseEntity.ok().build();
    }

    private ResponseEntity<Resource> doGet(Optional<GridFSFile> optFile, HttpHeaders requestHeaders) throws IOException {
        if (optFile.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        GridFSFile file = optFile.get();
        long lastModified = fileService.getUploadDate(file).truncatedTo(ChronoUnit.SECONDS).toEpochMilli();
        String etag = "\"" + fileService.getETag(file) + "\"";

        //prepare headers for response
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setETag(etag);
        responseHeaders.setLastModified(lastModified);

        long versionSince = requestHeaders.getIfModifiedSince();
        List<String> etagList = requestHeaders.getIfNoneMatch();

        //NOTE: it would be redundant to check that versionSince != -1(if no header)
        if (etagList.stream().anyMatch(x -> "*".equals(x) || x.equals(etag)) || versionSince >= lastModified) {
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).headers(responseHeaders).build();
        }
        Resource content = fileService.getAsResource(file);

        return ResponseEntity.ok().headers(responseHeaders).body(content);
    }
}
