package com.mongaRest.monga.mongoDb;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;

@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class SaveWithIdMongoEventListener extends AbstractMongoEventListener<Object> {

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Object> event) {
        final Object source = event.getSource();
        if (source instanceof BaseDocument) {
            if (((BaseDocument) source).getId() == null) {
                ((BaseDocument) source).setId(IdGenerator.nextId());
            }
        }
    }
}