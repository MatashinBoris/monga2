package com.mongaRest.monga.config;

import com.mongaRest.monga.mongoDb.MongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

@Configuration
public class CoreMongoConfig extends MongoConfig {

    @Override
    protected List<Converter<?, ?>> customConversions() {
        List<Converter<?, ?>> converterList = super.customConversions();
        return converterList;
    }

//    @Bean
//    public MongoClientOptions mongoOptions() {
//        return MongoClientOptions.builder().connectionsPerHost(200).maxConnectionIdleTime(60 * 1000).retryWrites(true).build();
//    }

//
//    @Bean
//    public com.fasterxml.jackson.databind.Module registerGeoJsonModule() {
//        return new GeoJsonModule();
//    }


}