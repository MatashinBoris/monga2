package com.mongaRest.monga.bean;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongaRest.monga.commons.DateDeserializer;
import com.mongaRest.monga.commons.DateSerializer;
import com.mongaRest.monga.commons.validator.InThePast;
import com.mongaRest.monga.commons.validator.ValidationConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class UserCreateBean {

    @JsonProperty
    @NotEmpty(message = ValidationConstants.USER_NICKNAME_MESSAGE)
    @Email(message = ValidationConstants.USER_NICKNAME_MESSAGE)
    @Size(min = 2, message = ValidationConstants.USER_NICKNAME_MESSAGE)
    private String nickName;

    @JsonProperty
    @NotEmpty(message = ValidationConstants.USER_LOGIN_MESSAGE)
    @Email(message = ValidationConstants.USER_LOGIN_MESSAGE)
    private String login;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotEmpty(message = ValidationConstants.USER_PASSWORD_MESSAGE)
    @Size(min = 8, message = ValidationConstants.USER_PASSWORD_MESSAGE)
    private String password;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_FIRST_NAME_MESSAGE)
    @Size(min = 1, max = 50, message = ValidationConstants.USER_FIRST_NAME_MESSAGE)
    private String firstName;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_LAST_NAME_MESSAGE)
    @Size(min = 1, max = 50, message = ValidationConstants.USER_LAST_NAME_MESSAGE)
    private String lastName;

    @JsonProperty
    @NotNull(message = ValidationConstants.USER_PHONE_MESSAGE)
    @Size(min = 10, message = ValidationConstants.USER_PHONE_MESSAGE)
    @Pattern(regexp = ValidationConstants.PHONE_PATTERN, message = ValidationConstants.USER_PHONE_MESSAGE)
    private String phone;

    @JsonProperty
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    @InThePast
    @NotNull
    protected OffsetDateTime birthDate;
}
