package com.mongaRest.monga.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.pojo.GeoPosition;
import com.mongaRest.monga.pojo.restaurantPojo.KitchenType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class RestaurantCreateBean {

    @JsonProperty(required = true)
    private String name;

    @JsonProperty
    private String city;

    @JsonProperty
    private String address;

    @JsonProperty
    private GeoPosition position;

    @JsonProperty(required = true)
    private List<KitchenType> kitchenTypes = new ArrayList<>();

    @JsonProperty
    private String phone;

    @JsonProperty
    private String email;

    @JsonProperty
    private String siteUrl;

    @JsonProperty
    private String description;

    @JsonProperty
    private int averageCheck;

    @JsonProperty
    private List<String> restaurantPhotos;
}
