package com.mongaRest.monga.repository;


import com.mongaRest.monga.bean.RestaurantSearchBean;
import com.mongaRest.monga.pojo.restaurantPojo.Restaurant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomRestaurantRepositoryImpl implements CustomRestaurantRepository{

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Page<Restaurant> search(RestaurantSearchBean search, Long creatorId, int page, int pageSize) {

        Query query = new Query();
        Criteria statusCriteria = null;

        if (search.getSortDirection() != null) {
            query.with(Sort.by(search.getSortDirection()));
        } else {
            query.with(Sort.by(Sort.Direction.DESC, Restaurant.RATING_FIELD));
        }

        if (StringUtils.isNotBlank(search.getSearchQuery())) {
            TextCriteria containsText = TextCriteria.forDefaultLanguage().matching(search.getSearchQuery());
            query = TextQuery.queryText(containsText).sortByScore();
        }

        if (creatorId != null) {
            Criteria criteria = Criteria.where(Restaurant.CREATOR_FIELD + ".$id").is(creatorId);
            query.addCriteria(criteria);

            Criteria activeCriteria = Criteria.where(Restaurant.ACTIVE_FIELD).ne(false);
            query.addCriteria(activeCriteria);
        }

        //search by work time
       /* if (search.getStartDay() != null && search.getEndDay() != null) {
            var startDate = search.getStartDay();
            var finishDate = search.getEndDay();

            Criteria criteria = new Criteria();
            criteria.andOperator(Criteria.where(Order.START_DATE_FIELD).gte(startDate), Criteria.where(Order.START_DATE_FIELD).lte(finishDate));
            query.addCriteria(criteria);
        }*/


        long total = mongoTemplate.count(query, Restaurant.class);
        query.skip((long) page * pageSize).limit(pageSize);
        List<Restaurant> results = mongoTemplate.find(query, Restaurant.class);
        return new PageImpl<>(results, PageRequest.of(page, pageSize), total);
    }
}
