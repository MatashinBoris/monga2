package com.mongaRest.monga.pojo.restaurantPojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.mongoDb.BaseDocument;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "menu")
@TypeAlias("menu")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ToString
public class Menu extends BaseDocument implements Serializable {

    @JsonProperty
    private List<MenuCategory> menuCategories = new ArrayList<>();
}
