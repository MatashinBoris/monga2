package com.mongaRest.monga.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.http.MediaType;

import java.util.Objects;

@Getter
@Accessors(chain = true)
public class UploadFileBean {

    @JsonProperty
    private String id;

    @JsonProperty
    @JsonSerialize(using = ToStringSerializer.class)
    private MediaType contentType;

    @JsonCreator
    public UploadFileBean(
            @JsonProperty(value = "id", required = true) String id,
            @JsonProperty(value = "contentType", required = true) String contentType) {
        this(id, MediaType.parseMediaType(contentType));
    }

    public UploadFileBean(String id, MediaType contentType) {
        this.id = Objects.requireNonNull(id, "File id can not be null");
        this.contentType = Objects.requireNonNull(contentType, "content type can not be null");
    }

}
