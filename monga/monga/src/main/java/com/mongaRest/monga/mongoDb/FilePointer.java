package com.mongaRest.monga.mongoDb;

import com.google.common.net.MediaType;

import java.io.InputStream;
import java.time.Instant;
import java.util.Optional;

public interface FilePointer {

    String getId();

    InputStream open();

    long getSize();

    String getOriginalName();

    String getEtag();

    Optional<MediaType> getMediaType();

    boolean matchesEtag(String requestEtag);

    Instant getLastModified();

    boolean modifiedAfter(Instant isModifiedSince);
}
