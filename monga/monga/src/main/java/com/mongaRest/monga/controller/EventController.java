package com.mongaRest.monga.controller;

import com.mongaRest.monga.bean.RestaurantCreateBean;
import com.mongaRest.monga.commons.token.Token;
import com.mongaRest.monga.pojo.EventPost;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ControllerAPI.EVENT_CONTROLLER + ControllerAPI.VERSION_V1)
@Slf4j
public class EventController {

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventPost> create (
        @RequestHeader(value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token,
        @RequestBody @Valid PostCreateBean createBean
    ) {

        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
}
