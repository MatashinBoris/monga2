package com.mongaRest.monga.commons.token;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongaRest.monga.pojo.RoleName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Token {

    public enum TokenType {
        USER, LOST_PASSWORD
    };

    public static final String FIELD_ID = "id";

    public static final String FIELD_LOGIN = "uid";

    public static final String FIELD_ROLE = "role";

    public static final String FIELD_TYPE = "type";

    public static final String COOKIE_KEY_FIELD = "accessToken";

    public static final String TOKEN_HEADER = "Authorization";

    public static final String SYNC_TOKEN_HEADER = "sync_token";

    public static final String COOKIE_DEFAULT_VALUE = "";

    @JsonProperty
    private String server;

    @JsonProperty
    private Long id;

    @JsonProperty
    private String uid;

    @JsonProperty
    private TokenType type;

    @JsonProperty
    private RoleName role;

}
