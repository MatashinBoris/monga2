package com.mongaRest.monga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@SpringBootApplication
@EnableMongoRepositories
public class MongaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongaApplication.class, args);
	}

}
