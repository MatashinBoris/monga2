package com.mongaRest.monga.mongoDb;


import com.mongaRest.monga.mongoDb.converter.DateToOffsetDateTimeConverter;
import com.mongaRest.monga.mongoDb.converter.OffsetDateTimeToDateConverter;
import com.mongaRest.monga.mongoDb.converter.OffsetTimeConverter;
import com.mongodb.DBRef;
import lombok.extern.slf4j.Slf4j;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.util.TypeInformation;
import org.springframework.lang.Nullable;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MongoConfig {

    public static final String ENGLISH = "english";

    public static final String RUSSIAN = "russian";

    @Autowired
    private MongoDatabaseFactory mongoDatabaseFactory;

    @Autowired
    private MongoMappingContext mongoMappingContext;

//	@Bean
//	public MongoClientSettings mongoOptions() {
//		MongoClientSettings.Builder settings = MongoClientSettings.builder().retryWrites(true);
//		settings.applyToSslSettings(builder -> builder.enabled(false));
//		settings.applyToConnectionPoolSettings(builder -> builder.maxSize(200).maxWaitTime(60, TimeUnit.SECONDS).maxConnectionIdleTime(60, TimeUnit.SECONDS).maxConnectionLifeTime(60, TimeUnit.SECONDS));
//		return settings.build();
//	}

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDatabaseFactory, mongoConverter());
        return mongoTemplate;
    }

//	@Bean
//	public MongoTransactionManager transactionManager() {
//		return new MongoTransactionManager(mongoDatabaseFactory);
//	}

    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDatabaseFactory);
        mongoMappingContext.setAutoIndexCreation(true);
        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mongoMappingContext) {

            @Override
            protected DBRef createDBRef(Object target, MongoPersistentProperty property) {
                // we should set id before save
                if (target instanceof BaseDocument) {
                    if (((BaseDocument) target).getId() == null) {
                        ((BaseDocument) target).setId(IdGenerator.nextId());
                    }
                }
                return super.createDBRef(target, property);
            }

            protected void writeInternal(@Nullable Object obj, Bson bson, @Nullable TypeInformation<?> typeHint) {
                // we should set id before save
                log.debug("Write internal for %s", obj);
                if (obj instanceof BaseDocument) {
                    if (((BaseDocument) obj).getId() == null) {
                        ((BaseDocument) obj).setId(IdGenerator.nextId());
                    }
                    if (((BaseDocument) obj).getCreatedDate() == null) {
                        ((BaseDocument) obj).setCreatedDate(OffsetDateTime.now(ZoneOffset.UTC));
                    }
                    ((BaseDocument) obj).setUpdatedDate(OffsetDateTime.now(ZoneOffset.UTC));
                }
                super.writeInternal(obj, bson, typeHint);
            }

        };

        mongoConverter.setCustomConversions(new MongoCustomConversions(customConversions()));

        return mongoConverter;
    }

    protected List<Converter<?, ?>> customConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<Converter<?, ?>>();
        converterList.add(new DateToOffsetDateTimeConverter());
        converterList.add(new OffsetDateTimeToDateConverter());
        converterList.add(new OffsetTimeConverter.Serializer());
        converterList.add(new OffsetTimeConverter.Deserializer());
        return converterList;
    }

    @Bean
    public SaveWithIdMongoEventListener saveWithIdMongoEventListener() {
        return new SaveWithIdMongoEventListener();
    }

    @Bean
    public CascadeSaveMongoEventListener cascadeSaveMongoEventListener() {
        return new CascadeSaveMongoEventListener();
    }
}