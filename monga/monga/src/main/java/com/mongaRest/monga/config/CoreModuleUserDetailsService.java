package com.mongaRest.monga.config;


import com.mongaRest.monga.pojo.RoleName;
import com.mongaRest.monga.repository.UserRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Slf4j
@NoArgsConstructor
public class CoreModuleUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        log.info("TEST REQUEST - %s", login);
        com.mongaRest.monga.pojo.User domainUser = userRepository.findByLogin(login);

        if (domainUser == null) {
            throw new UsernameNotFoundException("User not found");
        }

        if (domainUser.getBlocked()) {
            log.info("User %s was blocked", login);
            throw new UsernameNotFoundException("User blocked");
        }

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        log.info(" USER ROLE- %s ", getAuthorities(domainUser.getUserRoles()));
        User newUser = new User(domainUser.getLogin(), domainUser.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthorities(domainUser.getUserRoles()));
        log.info(" USER FROM SERVICE - %s   autority = %s", newUser, newUser.getAuthorities());
        return newUser;
    }


    public Collection<GrantedAuthority> getAuthorities(Set<RoleName> userRoles) {
        List<String> roles = getRoles();

        List<String> castUserRoles = new ArrayList<>();

        for (RoleName str : userRoles) {

            castUserRoles.add(str.name());
        }

        List<GrantedAuthority> grantedAuthorities = getGrantedAuthorities(roles, castUserRoles);
        return grantedAuthorities;
    }

    public List<String> getRoles() {
        var roles = new ArrayList<String>();
        for (RoleName role : RoleName.values()) {
            roles.add(role.name());
            log.info("  Check ROLE = %s ", role);
        }
        return roles;
    }



    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles, List <String> userRoles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

        for (String userRole : userRoles) {

            for (String role : roles) {

                if (role.equals(userRole)) {
                    SimpleGrantedAuthority simpleAuth = new SimpleGrantedAuthority("ROLE_".concat(role));
                    log.info("  Simple  AUTH = %s ", simpleAuth);
                    grantedAuthorities.add(simpleAuth);
                }

            }
        }

        return grantedAuthorities;
    }
}
