package com.mongaRest.monga.controller;

import com.mongaRest.monga.bean.UserCreateBean;
import com.mongaRest.monga.bean.UserUpdateBean;
import com.mongaRest.monga.commons.token.Token;
import com.mongaRest.monga.commons.token.TokenUser;
import com.mongaRest.monga.config.security.jwt.JwtTokenProvider;
import com.mongaRest.monga.exception.BadRequestException;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import com.mongaRest.monga.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(ControllerAPI.USER_CONTROLLER + ControllerAPI.VERSION_V1)
@Slf4j
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> signupUser(
            HttpServletRequest request,
            @RequestBody @Valid UserCreateBean signupUserBean
    ) {
        userService.checkUserAge(signupUserBean.getBirthDate());
        userService.checkUserMailServer(signupUserBean.getLogin());

        User user = userService.createUser(signupUserBean);

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(value = ControllerAPI.USER_CONTROLLER_GET + ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getById(
            @PathVariable(name = "id") Long id,
            @RequestHeader(value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token
    ) throws BadRequestException {

      //  TokenUser tokenUser = getClientFromTokenOrThrow(token);
        System.out.println(id);

       // userRepository.save(user);
        User user = new User().setLastName("oLOLOOLo").setFirstName("ololo");

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(
            @PathVariable("id") Long id,
            @RequestHeader(value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token,
            @RequestBody @Valid UserUpdateBean updateBean
    ) {

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PutMapping(value = ControllerAPI.USER_CONTROLLER_BLOCK_USER + ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> blockUser(
            @PathVariable("id") Long id,
            @RequestHeader (value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token
    ) {

        return new ResponseEntity<>(null,  HttpStatus.OK);
    }

    @PutMapping(value = ControllerAPI.USER_CONTROLLER_UNBLOCK_USER + ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> unblockUser(
            @PathVariable("id") Long id,
            @RequestHeader (value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token
    ) {

        return new ResponseEntity<>(null,  HttpStatus.OK);
    }


    @DeleteMapping(value = ControllerAPI.CONTROLLER_SPECIFIC_REQUEST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> deleteUser(
            @PathVariable ("id") Long id,
            @RequestHeader(value = Token.TOKEN_HEADER, defaultValue = Token.COOKIE_DEFAULT_VALUE) String token
    ) {

        return null;
    }


    private TokenUser getClientFromTokenOrThrow(String token) throws BadRequestException {
        return  jwtTokenProvider.decodeToken(token);
    }
}
