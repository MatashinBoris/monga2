package com.mongaRest.monga.service;

import com.mongaRest.monga.bean.UserCreateBean;
import com.mongaRest.monga.commons.validator.ValidationConstants;
import com.mongaRest.monga.config.security.jwt.JwtTokenProvider;
import com.mongaRest.monga.exception.ValidationException;
import com.mongaRest.monga.pojo.RoleName;
import com.mongaRest.monga.pojo.User;
import com.mongaRest.monga.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    private static final List<String> BANNED_EMAIL_SERVICES = Arrays.asList("mail.ru", "rambler.ru", "yandex.ru", "10minute.com", "mailinator.com");

    private static final List<String> BANNED_EMAIL_ZONES = Arrays.asList("ru", "рф");

    @Autowired
    private JwtTokenProvider tokenUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    public User createUser(UserCreateBean signupUserBean) {

        User byLogin = userRepository.findByLogin(signupUserBean.getLogin());
        if (byLogin != null) {
            throw new ValidationException(User.LOGIN_FIELD, "Email is already exist");
        }

        User byNickName = userRepository.findByNickName(signupUserBean.getNickName());
        if (byNickName != null) {
            throw new ValidationException(User.LOGIN_FIELD, "Nickname is already exist");
        }

        User user = new User();

        user.setLogin(signupUserBean.getLogin());
        user.setNickName(signupUserBean.getNickName());

        user.setFirstName(signupUserBean.getFirstName());
        user.setPassword(passwordEncoder.encode(signupUserBean.getPassword().trim().toLowerCase()));

        user.setLastName(signupUserBean.getLastName());
        user.setUserRoles(Set.of(RoleName.USER));

        user.setPhone(signupUserBean.getPhone());
        user.setBirthDate(signupUserBean.getBirthDate());

        return userRepository.save(user);
    }

    public void checkUserAge(OffsetDateTime birthDate) {
        if (birthDate != null && birthDate.plusYears(ValidationConstants.MINIMAL_USER_AGE).isAfter(OffsetDateTime.now())) {
            throw new ValidationException(User.BIRTHDATE_FIELD, ValidationConstants.MINIMAL_USER_AGE_MESSAGE);
        }
        if (birthDate != null && birthDate.plusYears(ValidationConstants.MAXIMUM_USER_AGE).isBefore(OffsetDateTime.now())) {
            throw new ValidationException(User.BIRTHDATE_FIELD, ValidationConstants.MAXIMUM_USER_AGE_MESSAGE);
        }
    }

    public void checkUserMailServer(String email) {
        String emailServer = email.substring(email.indexOf("@") + 1);
        if (BANNED_EMAIL_SERVICES.contains(emailServer)) {
            throw new ValidationException(User.LOGIN_FIELD, ValidationConstants.BANNED_EMAIL_SERVICE_MESSAGE);
        }
        emailServer = email.substring(email.lastIndexOf(".") + 1);
        if (BANNED_EMAIL_ZONES.contains(emailServer)) {
            throw new ValidationException(User.LOGIN_FIELD, ValidationConstants.BANNED_EMAIL_SERVICE_MESSAGE);
        }
    }



}
